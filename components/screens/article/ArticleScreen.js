import React, { Component } from 'react'
import { StyleSheet, View, FlatList, Image, Text, ActivityIndicator } from 'react-native'
import { RkButton, RkCard, RkText } from 'react-native-ui-kitten'
import { Bubbles } from 'react-native-loader'

export default class ArticleScreen extends Component{

	constructor(){
		super()
		this.state = {
			article: [],
			isLoading: true
		}
	}

	componentDidMount(){
		const url = "http://ngodingers.com/api/v1/article/all"
		fetch(url)
		.then( (res) => res.json() )
		.then( (resJson) => {
			this.setState({
				article: resJson.data.data,
				isLoading: false
			})
		})
		.catch( (err) => {
			console.log(err)
		})
	}

	renderItem = ({ key, item }) => {
		return(
			<RkCard rkType="shadowed" style={{ marginBottom: 6 }}>

				<View rkCardHeader>
					<Text style={{ fontWeight: 'bold' }} >{ item.title }</Text>
				</View>

				<Image rkCardImg
					source={{ uri: 'http://ngodingers.com/ngodingers/storage/app/public/article/' + item.cover }}
				/>

				<View rkCardContent>
					<Text>{ item.caption }</Text>
				</View>

				<View rkCardFooter>
					<RkButton rkType="danger rounded" onPress={ () => this.props.navigation.navigate('DetailArticle')}>Read More </RkButton>
				</View>

			</RkCard>
		)
	}

	render(){
		return(
			this.state.isLoading
			?
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<Bubbles size={16} color="#C82D3D" />
			</View>
			:
			<View style={ styles.container }>
				<FlatList
					data={this.state.article}
					renderItem={this.renderItem}
					keyExtractor={(item, index) => index.toString()}
				/>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 10
	}
})