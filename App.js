import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation'
import Icon from 'react-native-vector-icons/Ionicons'

//screen
import ArticleScreen from './components/screens/article/ArticleScreen'
import DetailArticleScreen from './components/screens/article/DetailArticleScreen'

import AcademyScreen from './components/screens/academy/AcademyScreen'
import DetailAcademyScreen from './components/screens/academy/DetailAcademyScreen'

import NewsScreen from './components/screens/news/NewsScreen'
import TutorialScreen from './components/screens/tutorial/TutorialScreen'

export default class App extends React.Component {

  constructor(){
    super()
    this.state = {}
  }

  render() {
    return (
      <AppStackNavigator />
    );
  }
}


const AppStackNavigator = createStackNavigator({
  Home: {
    screen: createBottomTabNavigator({
      Article: {
        screen: ArticleScreen,
        navigationOptions: {
          tabBarLabel: 'Article',
          tabBarIcon: ({ tintColor }) => (
            <Icon name="md-compass" color={tintColor} size={26} />
          )
        }
      },
      Tutorial: {
        screen: TutorialScreen,
        navigationOptions: {
          tabBarLabel: 'Tutorial',
          tabBarIcon: ({ tintColor }) => (
            <Icon name="md-code-working" color={tintColor} size={26} />
          )
        }
      },
      News: {
        screen: NewsScreen,
        navigationOptions: {
          tabBarLabel: 'News',
          tabBarIcon: ({ tintColor }) => (
            <Icon name="md-book" color={tintColor} size={26} />
          )
        }
      },
      Academy: {
        screen: AcademyScreen,
        navigationOptions:{
          tabBarLabel: 'Academy',
          tabBarIcon: ({ tintColor }) => (
            <Icon name="ios-clipboard" color={tintColor} size={26} />
          )
        }
      }
    })
  },
  DetailAcademy: {
    screen: DetailAcademyScreen
  },
  DetailArticle: {
    screen: DetailArticleScreen
  }
},{
  navigationOptions: {
    title: 'Ngodingers',
    headerStyle:{
      backgroundColor: '#C82D3D',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    }
  }
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
